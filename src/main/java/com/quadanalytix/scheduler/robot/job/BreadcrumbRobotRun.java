package com.quadanalytix.scheduler.robot.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.kapowtech.robosuite.api.java.rql.RQLException;
import com.kapowtech.robosuite.api.java.rql.RQLObjectBuilder;
import com.kapowtech.robosuite.api.java.rql.RQLResult;
import com.kapowtech.robosuite.api.java.rql.Request;
import com.kapowtech.robosuite.api.java.rql.construct.RQLObject;
import com.kapowtech.robosuite.api.java.rql.construct.RQLObjects;
import com.kapowtech.robosuite.api.java.rql.construct.RepositoryRobotLibrary;

public class BreadcrumbRobotRun implements Job {

    public void execute(final JobExecutionContext context)
            throws JobExecutionException {
        final JobDataMap dataMap = context.getMergedJobDataMap();
        final int count = dataMap.getIntValue("count");
        final String tempdir = dataMap.getString("tempdir");
        final String getURI = dataMap.getString("getURI");
        final String storeURI = dataMap.getString("storeURI");
        final String robotName = dataMap.getString("robotName");
        final String itemCategory = dataMap.getString("ItemCategory");
        final String merchant = dataMap.getString("Merchant");
        final String repositoryURI = dataMap.getString("repositoryURI");
        final String breadcrumbProject = dataMap.getString("breadcrumbProject");
        final String clusterName = dataMap.getString("clusterName");
        System.out.println(count + "," + tempdir + "," + getURI + ","
                + storeURI + "," + robotName + "," + itemCategory + ","
                + merchant);

        final Request request = new Request("Library:/" + robotName);
        request.setStopOnConnectionLost(true);
        request.setStopRobotOnApiException(true);
        request.setMaxExecutionTime(3600);

        final RQLObjectBuilder inputObject = request
                .createInputVariable("input");
        inputObject.setAttribute("merchantid", (String) merchant);
        inputObject.setAttribute("timeStamp", (String) null);
        inputObject.setAttribute("robotCompleted_URL", (String) null);
        inputObject.setAttribute("tempdir", (String) tempdir);
        inputObject.setAttribute("getProdURIs_URL", (String) getURI);
        inputObject.setAttribute("storeSeedURIs_URL", (String) storeURI);
        inputObject.setAttribute("itemcatid", itemCategory);

        final RepositoryRobotLibrary lib = new RepositoryRobotLibrary(
                repositoryURI, breadcrumbProject, 60000L, null, null);
        request.setRobotLibrary(lib);
        RQLResult result;

        try {
            result = request.execute(clusterName);
            final RQLObjects kitchenShelfs = result
                    .getOutputObjectsByName("kitchenShelf");
            if (kitchenShelfs.size() != 0) {
                for (int i = 0; i < kitchenShelfs.size(); i++) {
                    final RQLObject kitchenShelf = kitchenShelfs.getObject(i);
                    final Long merchantId = (Long) kitchenShelf
                            .get("merchantId");
                    final Long itemCatId = (Long) kitchenShelf.get("itemCatId");
                    final String shelfUrl = (String) kitchenShelf
                            .get("shelfURL");
                    System.out.println(merchantId + "~" + itemCatId + "~"
                            + shelfUrl);
                }
            }
        } catch (final RQLException e) {
            throw new JobExecutionException(
                    "Breadcrumb Robot Exception for merchant " + merchant
                            + " and itemCategory " + itemCategory);
        }
    }

}
