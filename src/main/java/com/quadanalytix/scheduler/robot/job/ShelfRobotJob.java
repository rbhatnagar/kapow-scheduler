package com.quadanalytix.scheduler.robot.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kapowtech.robosuite.api.java.rql.RQLException;
import com.kapowtech.robosuite.api.java.rql.RQLObjectBuilder;
import com.kapowtech.robosuite.api.java.rql.Request;
import com.kapowtech.robosuite.api.java.rql.construct.RepositoryRobotLibrary;
import com.kapowtech.robosuite.api.java.rql.construct.RobotLibrary;

import static com.quadanalytix.scheduler.config.QuadRoboConstants.*;
public class ShelfRobotJob implements Job {

    private static final Logger LOG = LoggerFactory.getLogger(ShelfRobotJob.class);
    public ShelfRobotJob() {

    }

    public void execute(final JobExecutionContext context) throws JobExecutionException {

        
        final JobDataMap dataMap = context.getMergedJobDataMap();

        // Request Generation Assigning Input Variables
        final Request request = new Request("Library:/" + dataMap.getString(ROBOTNAME));
        
        //Robot Input Parameter
        final RQLObjectBuilder inputObject = request.createInputVariable("input");

        inputObject.setAttribute("merchantid", dataMap.getString(MERCHANTID));
        inputObject.setAttribute("robotCompleted_URL", dataMap.getString(ROBOTCOMPLETEDURL));
        inputObject.setAttribute("tempdir", dataMap.getString(TEMPDIR));
        inputObject.setAttribute("timeStamp", dataMap.getString(TIMESTAMP));
        inputObject.setAttribute("getShelfURIs_URL", dataMap.getString(GETURI));
        inputObject.setAttribute("storeSeedURIs_URL", dataMap.getString(STOREURI));
        inputObject.setAttribute("itemcatid", dataMap.getString(ITEMCATEGORYID));

        // Set Robot Parameters
        request.setStopOnConnectionLost(true);
        request.setStopRobotOnApiException(false);
        request.setMaxExecutionTime(7200); // Set to 2 hours
        
        
        final RobotLibrary library = new RepositoryRobotLibrary(dataMap.getString(REPOSITORYURL), dataMap.getString(PROJECT), 60000L, null, null);
        request.setRobotLibrary(library);
        try {
            
            request.execute(dataMap.getString(CLUSTERNAME));
        } catch (final RQLException e) {

               LOG.error(context.getTrigger().getJobKey().getName() + e.getMessage());
               throw new JobExecutionException("RQL Exception");
               
        }

    }

}
