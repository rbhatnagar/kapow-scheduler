package com.quadanalytix.scheduler.robot.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kapowtech.robosuite.api.java.repository.construct.RoboServer;
import com.kapowtech.robosuite.api.java.repository.engine.RepositoryClientException;
import com.kapowtech.robosuite.api.java.rql.engine.hotstandby.ExecutorLogger;

public class RobotExecutorLogger implements ExecutorLogger { 
    
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RobotExecutorLogger.class);

    
    public void onNotEnoughStandbyServers(final int paramInt1, final int paramInt2) {
        
    }
    
    public void onInvalidLicense(final RoboServer paramRoboServer, final String paramString) {
        
    }

    public void onShutDownNotCalled(final String paramString) {
    }
    

    public void onRepositoryException(final RepositoryClientException paramRepositoryClientException) {
    
    }

    public void onServerOffline(final RoboServer paramRoboServer, final String paramString) {
         LOGGER.error(paramRoboServer.toString() + " went Offline");
    }

}
