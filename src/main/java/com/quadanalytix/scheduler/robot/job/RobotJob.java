/**
 * 
 */
package com.quadanalytix.scheduler.robot.job;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kapowtech.robosuite.api.java.repository.construct.Cluster;
import com.kapowtech.robosuite.api.java.repository.construct.RoboServer;
import com.kapowtech.robosuite.api.java.rql.ClusterAlreadyDefinedException;
import com.kapowtech.robosuite.api.java.rql.Request;
import com.kapowtech.robosuite.api.java.rql.engine.hotstandby.RequestExecutor;
import com.quadanalytix.scheduler.config.QuadRoboConfig;

import static com.quadanalytix.scheduler.config.QuadRoboConstants.*;

/**
 * @author Faij
 * 
 */
public final class RobotJob {

    private static final QuadRoboConfig ROBOCONFIG = QuadRoboConfig.getInstance();
    private static final Logger LOGGER = LoggerFactory.getLogger(RobotJob.class);
    
    

    private RobotJob() {
        super();
    }

    public static void registerCluster() throws ClusterAlreadyDefinedException, RoboServerException {

        final String[] roboserverurls = ROBOCONFIG.getProperty(ROBOSERVER).split(SEPARATOR);
        URI uri;

        final List<RoboServer> robolist = new ArrayList<RoboServer>();

        for (int i = 0; i < roboserverurls.length; i++) {

            uri = URI.create(roboserverurls[i]);

            robolist.add(new RoboServer(uri.getHost(), uri.getPort()));

            LOGGER.info("Adding " + uri.getHost() + ":" + uri.getPort() + " to Robo Server List");

        }

        final boolean ssl = false;
        final Cluster cluster = new Cluster(ROBOCONFIG.getProperty(CLUSTERNAME), robolist.toArray(new RoboServer[robolist.size()]), ssl);

        final RobotExecutorLogger roboexecutorlogger = new RobotExecutorLogger();
            Request.registerCluster(cluster, roboexecutorlogger);
            final RequestExecutor executor = new RequestExecutor(cluster, roboexecutorlogger);      
            
            final int slots = executor.getTotalAvailableSlots();
            if (slots == 0) {
                throw new RoboServerException("RoboServers Not Registered 0 Slots Available");
            }
          
            LOGGER.info("Cluster Registered with : " + slots);


    }

}
