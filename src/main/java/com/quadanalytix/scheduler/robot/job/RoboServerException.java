package com.quadanalytix.scheduler.robot.job;

public class RoboServerException extends Exception {
    
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public RoboServerException(final String message) {
        super(message);
    }

    public RoboServerException(final String message, final Exception e) {
        super(message, e);
    }


}
