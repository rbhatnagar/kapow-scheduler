package com.quadanalytix.scheduler.robot.listener;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quadanalytix.config.QuadConfig;
import com.quadanalytix.dataaccess.hbase.EntityResourceHandler;
import com.quadanalytix.dataaccess.hbase.TimeSeriesHandler;
import com.quadanalytix.entities.mongo.Shelf;

public class RobotTriggerListener implements TriggerListener {
    
    private static final Logger LOG = LoggerFactory.getLogger(RobotTriggerListener.class);

    
    public static String LISTENER_NAME = "RobotTriggerListener";
    private static final TimeSeriesHandler TIME_SERIES_HANDLER = EntityResourceHandler.getResourceHandler(Shelf.class).getTsHandler();

    public RobotTriggerListener(String string) {
        // TODO Auto-generated constructor stub
        
        LISTENER_NAME=string;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return LISTENER_NAME;
    }
    
    @Override
    public void triggerFired(final Trigger trigger, final JobExecutionContext context) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean vetoJobExecution(final Trigger trigger, final JobExecutionContext context) {
        // check for the time boundary
        if (!TIME_SERIES_HANDLER.areInSameDataPointBoundary(context.getScheduledFireTime().getTime(), context.getFireTime().getTime()))
                {
            LOG.info("Vetoing Job Execution: " + context.getTrigger().getKey());
            return true;
                }
        return false;
    }

    @Override
    public void triggerMisfired(final Trigger trigger) {
        // TODO Auto-generated method stub
        LOG.info("Trigger Misfire: " + trigger.getKey());

    }

    @Override
    public void triggerComplete(final Trigger trigger, final JobExecutionContext context, final CompletedExecutionInstruction triggerInstructionCode) {
        // TODO Auto-generated method stub
        
    }
    
    

}
