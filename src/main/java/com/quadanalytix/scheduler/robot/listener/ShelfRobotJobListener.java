package com.quadanalytix.scheduler.robot.listener;

import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import static org.quartz.DateBuilder.*;

public class ShelfRobotJobListener implements JobListener {
    
    
    
    private static final Logger LOG = LoggerFactory.getLogger(ShelfRobotJobListener.class);
    private String mname;

    public ShelfRobotJobListener(final String name) {
        mname = name;
    }

    public String getName() {
        return mname;
    }

    public void jobToBeExecuted(final JobExecutionContext context) {

        LOG.info(mname + "-" + context.getTrigger().getKey().getName() + " to be executed");
    }

    public void jobWasExecuted(final JobExecutionContext context, final JobExecutionException jobException) {

        LOG.info(mname + "-" + context.getTrigger().getKey().getName() + " was executed");

        // check for retry
        
      if (jobException.getMessage() == "RQL Excetpion") {
       
      final JobDetail job = context.getJobDetail();
        final Trigger trigger = context.getTrigger();

       
         
        final Trigger retrigger = (SimpleTrigger) newTrigger().withIdentity(trigger.getKey().getName() + "_retry", "Shelf").startAt(futureDate(1, IntervalUnit.MINUTE))
              .usingJobData(trigger.getJobDataMap()).forJob(job.getKey()).build();
        

        try {

            final Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.scheduleJob(retrigger);
            

             } catch (final SchedulerException e) {
            System.out.println(e.getMessage());
             }
      } 
        // do something with the event
    }

    public void jobExecutionVetoed(final JobExecutionContext context) {

        LOG.info(mname + context.getTrigger().getKey() + " execution vetoed");

        // do something with the event
    }

}
