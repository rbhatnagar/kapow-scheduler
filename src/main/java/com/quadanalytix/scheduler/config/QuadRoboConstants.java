package com.quadanalytix.scheduler.config;

public final class QuadRoboConstants {
    
    private QuadRoboConstants() {
        
    }
    public static final String TEMPDIR = "tempdir"; 
    public static final String GETURI = "getURI";
    public static final String STOREURI = "storeURI";
    public static final String PROJECT = "project";
    public static final String ROBOTNAME = "roboname";
    public static final String ITEMCATEGORYID = "itemcategoryid";
    public static final String TIMESTAMP = "timestamp";
    public static final String MERCHANTID = "merchantid";
    public static final String REPOSITORYURL = "repositoryURL";
    public static final String ROBOTCOMPLETEDURL = "robotCompletedURL";
    public static final String CLUSTERNAME = "clustername";
    public static final String ROBOSERVER = "roboserver";
    public static final String SEPARATOR = ",";
    

   
}
