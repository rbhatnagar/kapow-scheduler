package com.quadanalytix.scheduler.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public final class QuadRoboConfig {

    private static Properties sROBOCONFIG = new Properties();
    private static final String ROBO_CONFIG_FILE = "/etc/quad/robot.config";
    private static final Logger LOG = LoggerFactory.getLogger(QuadRoboConfig.class);
 
    private static QuadRoboConfig sInstance;

    private QuadRoboConfig() {
        InputStream input = null;

        try {
            input = new FileInputStream(ROBO_CONFIG_FILE);
            sROBOCONFIG.load(input);
        } catch (final IOException ex) {
            LOG.error("IO Error in loading Quad Robo config file");
        }
 
    }
    
    public static QuadRoboConfig getInstance() {
        if (sInstance == null) {
            createInstance();
        }
        return sInstance;
    }

    public static synchronized void createInstance() {
        if (sInstance == null) {
            LOG.info("Loading Robo Config File");
            sInstance = new QuadRoboConfig();
        }

    }

     public String getProperty(final String key) {
         return sROBOCONFIG.getProperty(key);
     }
    
}
