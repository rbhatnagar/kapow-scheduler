package com.quadanalytix.scheduler.quartz.job;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.quartz.Trigger.CompletedExecutionInstruction;

import com.quadanalytix.dataaccess.hbase.EntityResourceHandler;
import com.quadanalytix.dataaccess.hbase.TimeSeriesHandler;
import com.quadanalytix.entities.mongo.Shelf;

public class RobotTriggerListener implements TriggerListener {
    
    public static final String LISTENER_NAME = "RobotTriggerListener";
    private static final TimeSeriesHandler TIME_SERIES_HANDLER = EntityResourceHandler.getResourceHandler(Shelf.class).getTsHandler();

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void triggerFired(final Trigger trigger, final JobExecutionContext context) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean vetoJobExecution(final Trigger trigger, final JobExecutionContext context) {
        // check for the time boundary
        if (!TIME_SERIES_HANDLER.areInSameDataPointBoundary(context.getScheduledFireTime().getTime(), context.getFireTime().getTime()))
                {
            System.out.println("Vetoing Job Execution: " + context.getTrigger().getKey());
            return true;
                }
        return false;
    }

    @Override
    public void triggerMisfired(final Trigger trigger) {
        // TODO Auto-generated method stub
        System.out.println("Trigger Misfire: " + trigger.getKey());

    }

    @Override
    public void triggerComplete(final Trigger trigger, final JobExecutionContext context, final CompletedExecutionInstruction triggerInstructionCode) {
        // TODO Auto-generated method stub
        
    }
    
    

}
