/**
 * 
 */
package com.quadanalytix.scheduler.quartz.job;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.impl.matchers.GroupMatcher.jobGroupEquals;

import java.util.List;
import java.util.Properties;

import org.mongodb.morphia.Datastore;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import com.quadanalytix.entities.mongo.ItemCategory;
import com.quadanalytix.persistence.mongo.MongoManager;
import com.quadanalytix.persistence.mongo.dao.ItemCategoryDAO;
import com.quadanalytix.scheduler.robot.job.BreadcrumbRobotRun;
import com.quadanalytix.scheduler.robot.job.ProductRobotRun;
import com.quadanalytix.scheduler.robot.job.ShelfRobotJob;

/**
 * @author Faij
 * 
 */
public final class QuartzScheduler {

    /**
     * 
     */

    private static int sintervalMinute = 41;
    private static int sintervalHour = 17;

    private static final Datastore QUAD_CA_DATASTORE = MongoManager
            .getQuadCatalogDatastore();
    private static final ItemCategoryDAO ITEM_CATEGORY_DAO = new ItemCategoryDAO(
            QUAD_CA_DATASTORE);
    private static String svalueItem;
    private static int sflag;

    private QuartzScheduler() {
        super();
    }

    public static void startSchedule(final Properties roboConfig)
            throws SchedulerException {

        final Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        final JobDetail jdBreadcrumb = createJobDetailBreadcrumb("Breadcrumb",
                "Breadcrumb", "bd", scheduler, roboConfig);
        final JobDetail jdShelf = createJobDetailShelf("Shelf", "Shelf", "sh",
                scheduler, roboConfig);
        final JobDetail jdProduct = createJobDetailProduct("Product",
                "Product", "pd", scheduler, roboConfig);

        System.out.println(jdBreadcrumb.getKey().toString());
        System.out.println(jdShelf.getKey().toString());
        System.out.println(jdProduct.getKey().toString());

        createTriggerBreadcrumb(jdBreadcrumb, scheduler, roboConfig);
        createTriggerShelf(jdShelf, scheduler, roboConfig);
        createTriggerProduct(jdProduct, scheduler, roboConfig);

        scheduler.getListenerManager().addJobListener(
                new JobListenersBreadcrumb(), jobGroupEquals("Breadcrumb"));
        scheduler.getListenerManager().addJobListener(new JobListenersShelf(),
                jobGroupEquals("Shelf"));
        scheduler.getListenerManager().addJobListener(
                new JobListenersProduct(), jobGroupEquals("Product"));
        scheduler.start();

    }

    private static JobDetail createJobDetailBreadcrumb(final String jobName,
            final String groupName, final String type,
            final Scheduler scheduler, final Properties roboConfig)
            throws SchedulerException {

        JobDetail jobDetail = null;

        final JobKey jk = new JobKey(jobName, groupName);
        if (!scheduler.checkExists(jk)) {
            jobDetail = newJob(BreadcrumbRobotRun.class)
                    .withIdentity(jobName, groupName)
                    .usingJobData("tempdir", roboConfig.getProperty("tempdir"))
                    .usingJobData("getURI",
                            roboConfig.getProperty("getProdURI") + type)
                    .usingJobData("storeURI",
                            roboConfig.getProperty("storeSeedURI") + type)
                    .usingJobData("breadcrumbProject",
                            roboConfig.getProperty("breadcrumbProject"))
                    .storeDurably().build();
            scheduler.addJob(jobDetail, false);
        } else {
            jobDetail = scheduler.getJobDetail(jk);
        }

        return jobDetail;

    }

    private static JobDetail createJobDetailShelf(final String jobName,
            final String groupName, final String type,
            final Scheduler scheduler, final Properties roboConfig)
            throws SchedulerException {

        JobDetail jobDetail = null;

        final JobKey jk = new JobKey(jobName, groupName);
        if (!scheduler.checkExists(jk)) {
            jobDetail = newJob(ShelfRobotJob.class)
                    .withIdentity(jobName, groupName)
                    .usingJobData("tempdir", roboConfig.getProperty("tempdir"))
                    .usingJobData("getURI",
                            roboConfig.getProperty("getURI") + type)
                    .usingJobData("storeURI",
                            roboConfig.getProperty("storeURI") + type)
                    .usingJobData("shelfProject",
                            roboConfig.getProperty("shelfProduct"))
                    .storeDurably().build();
            scheduler.addJob(jobDetail, false);
        } else {
            jobDetail = scheduler.getJobDetail(jk);
        }

        return jobDetail;

    }

    private static JobDetail createJobDetailProduct(final String jobName,
            final String groupName, final String type,
            final Scheduler scheduler, final Properties roboConfig)
            throws SchedulerException {

        JobDetail jobDetail = null;

        final JobKey jk = new JobKey(jobName, groupName);
        if (!scheduler.checkExists(jk)) {
            jobDetail = newJob(ProductRobotRun.class)
                    .withIdentity(jobName, groupName)
                    .usingJobData("tempdir", roboConfig.getProperty("tempdir"))
                    .usingJobData("getProdURI",
                            roboConfig.getProperty("getProdURI") + type)
                    .usingJobData("storeSeedURI",
                            roboConfig.getProperty("storeSeedURI") + type)
                    .usingJobData("productProject",
                            roboConfig.getProperty("productProject"))
                    .storeDurably().build();
            scheduler.addJob(jobDetail, false);
        } else {
            jobDetail = scheduler.getJobDetail(jk);
        }

        return jobDetail;

    }

    private static void createTrigger(final JobDetail jobDetail,
            final Scheduler scheduler, final String itemCategory,
            final Object merchant, final String group, final int flag,
            final Properties roboConfig) throws SchedulerException {
        String robotName = "";

        if (roboConfig.getProperty(merchant.toString()) != null) {

            if (sflag == 1) {
                sintervalMinute += 5;
                if (sintervalMinute >= 60) {
                    sintervalMinute -= 60;
                    sintervalHour += 1;
                }
                if (sintervalHour >= 24) {
                    sintervalHour = 0;
                }
            }

            final String[] robotNames = roboConfig.getProperty(
                    merchant.toString()).split(",");
            if (group == "Breadcrumb") {
                robotName = robotNames[0];
            } else if (group == "Shelf") {
                robotName = robotNames[1];
            } else {
                robotName = robotNames[2];
            }

            Trigger trigger = null;
            
            final TriggerKey tk = new TriggerKey("Trigger_" + itemCategory
                    + "_" + merchant, group);
            if (!scheduler.checkExists(tk)) {
                trigger = newTrigger()
                        .withIdentity(
                                "Trigger_" + itemCategory + "_" + merchant,
                                group)
                        .withSchedule(
                                cronSchedule("0 " + sintervalMinute + " "
                                        + sintervalHour + " ? * TUES"))
                        .usingJobData("robotName", robotName)
                        .usingJobData("ItemCategory", itemCategory)
                        .usingJobData("Merchant", merchant.toString())
                        .usingJobData("repositoryURI",
                                roboConfig.getProperty("repositoryURI"))
                        .usingJobData("count", "0")
                        .usingJobData("clusterName",
                                roboConfig.getProperty("Cluster_name"))
                        .forJob(group, group).build();

                scheduler.scheduleJob(trigger);
            } else {
                trigger = scheduler.getTrigger(tk);
            }
        }

        // return trigger;
    }

    private static void createTriggerBreadcrumb(final JobDetail jdBreadcrumb,
            final Scheduler scheduler, final Properties roboConfig)
            throws SchedulerException {
        sflag = 0;
        svalueItem = "";
        final List<ItemCategory> itemCategories = ITEM_CATEGORY_DAO.getAll();
        for (final ItemCategory ic : itemCategories) {
            if (Integer.parseInt(ic.getQuadID()) > 0) {
                if (ic.getShelfIDMap().keySet() != null
                        && ic.getShelfIDMap().keySet().size() != 0) {
                    final Object[] merchant = ic.getShelfIDMap().keySet()
                            .toArray();
                    for (int i = 0; i < merchant.length; i++) {
                        if (svalueItem == ic.getQuadID()) {
                            sflag = 0;
                        } else {
                            sflag = 1;
                        }
                        svalueItem = ic.getQuadID();
                        createTrigger(jdBreadcrumb, scheduler, ic.getQuadID(),
                                merchant[i], "Breadcrumb", sflag, roboConfig);
                    }
                }
            }
        }
    }

    private static void createTriggerShelf(final JobDetail jdBreadcrumb,
            final Scheduler scheduler, final Properties roboConfig)
            throws SchedulerException {
        sflag = 0;
        svalueItem = "";
        final List<ItemCategory> itemCategories = ITEM_CATEGORY_DAO.getAll();
        for (final ItemCategory ic : itemCategories) {
            if (Integer.parseInt(ic.getQuadID()) > 0) {
                if (ic.getShelfIDMap().keySet() != null
                        && ic.getShelfIDMap().keySet().size() != 0) {
                    final Object[] merchant = ic.getShelfIDMap().keySet()
                            .toArray();
                    for (int i = 0; i < merchant.length; i++) {
                        if (svalueItem == ic.getQuadID()) {
                            sflag = 0;
                        } else {
                            sflag = 1;
                        }
                        svalueItem = ic.getQuadID();
                        createTrigger(jdBreadcrumb, scheduler, ic.getQuadID(),
                                merchant[i], "Shelf", sflag, roboConfig);
                    }
                }
            }
        }
    }

    private static void createTriggerProduct(final JobDetail jdBreadcrumb,
            final Scheduler scheduler, final Properties roboConfig)
            throws SchedulerException {
        sflag = 0;
        svalueItem = "";
        final List<ItemCategory> itemCategories = ITEM_CATEGORY_DAO.getAll();
        for (final ItemCategory ic : itemCategories) {
            if (Integer.parseInt(ic.getQuadID()) > 0) {
                if (ic.getShelfIDMap().keySet() != null
                        && ic.getShelfIDMap().keySet().size() != 0) {
                    final Object[] merchant = ic.getShelfIDMap().keySet()
                            .toArray();
                    for (int i = 0; i < merchant.length; i++) {
                        if (svalueItem == ic.getQuadID()) {
                            sflag = 0;
                        } else {
                            sflag = 1;
                        }
                        svalueItem = ic.getQuadID();
                        createTrigger(jdBreadcrumb, scheduler, ic.getQuadID(),
                                merchant[i], "Product", sflag, roboConfig);
                    }
                }
            }
        }
    }

}
