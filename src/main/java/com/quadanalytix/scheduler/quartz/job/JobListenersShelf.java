/**
 * 
 */
package com.quadanalytix.scheduler.quartz.job;

import static org.quartz.DateBuilder.futureDate;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;
import java.util.Properties;

import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.quadanalytix.cmp.hbase.dao.StateTrackerDAO;
import com.quadanalytix.cmp.hbase.entities.state.ShelfRobotStateTracker;
import com.quadanalytix.dataaccess.PersistenceException;
import com.quadanalytix.dataaccess.hbase.TSTimeStamp;
import com.quadanalytix.scheduler.main.ServiceStart;

/**
 * @author Faij
 * 
 */
public class JobListenersShelf implements org.quartz.JobListener {

    public static final String LISTENER_NAME = "JobListener";

    @Override
    public String getName() {
        return LISTENER_NAME;
    }

    @Override
    public void jobToBeExecuted(final JobExecutionContext context) {

    }

    @Override
    public void jobExecutionVetoed(final JobExecutionContext context) {

    }

    @Override
    public void jobWasExecuted(final JobExecutionContext context,
            final JobExecutionException jobException) {
        if (jobException != null) {
            final JobDataMap dataMap = context.getMergedJobDataMap();
            int count = dataMap.getInt("count");

            int interval;

            final Properties roboConfig = ServiceStart.loadConfig();
            final int maxRetryCount = Integer.parseInt(roboConfig
                    .getProperty("maxRetryCount"));

            count++;
            
            
            if (count <= maxRetryCount) {
                interval = Integer.parseInt(roboConfig.getProperty("retryCount"
                        + count));
                final Trigger retryTrigger = createTriggerRetry(context,
                        dataMap, count, interval);
                Scheduler scheduler;
                if (count == 1) {
                    try {
                        scheduler = StdSchedulerFactory.getDefaultScheduler();
                        scheduler.scheduleJob(retryTrigger);
                    } catch (final SchedulerException e) {
                        System.out
                                .println("Unable to instantiate the scheduler");
                    }
                } else {
                    try {
                        scheduler = StdSchedulerFactory.getDefaultScheduler();
                        scheduler.rescheduleJob(context.getTrigger().getKey(),
                                retryTrigger);
                    } catch (final SchedulerException e) {
                        System.out
                                .println("Unable to instantiate the scheduler");
                    }
                }
            }
        }
    }

    private Trigger createTriggerRetry(final JobExecutionContext context,
            final JobDataMap dataMap, final int count, final int interval) {
        final Trigger actualTrigger = context.getTrigger();
        final JobDetail actualJobDetail = context.getJobDetail();
        String triggerName = "";

        if (actualTrigger.getKey().getName().contains("_retry")) {
            triggerName = actualTrigger.getKey().getName();
        } else {
            triggerName = actualTrigger.getKey().getName() + "_retry";
        }

        final Trigger retryTrigger = (SimpleTrigger) newTrigger()
                .withIdentity((String) triggerName,
                        actualTrigger.getKey().getGroup())
                .startAt(futureDate(interval, IntervalUnit.MINUTE))
                .forJob(actualJobDetail.getKey())
                .usingJobData("count", count)
                .usingJobData("tempdir", dataMap.getString("tempdir"))
                .usingJobData("getURI", dataMap.getString("getURI"))
                .usingJobData("storeURI", dataMap.getString("storeURI"))
                .usingJobData("robotName", dataMap.getString("robotName"))
                .usingJobData("ItemCategory", dataMap.getString("ItemCategory"))
                .usingJobData("Merchant", dataMap.getString("Merchant"))
                .usingJobData("repositoryURI",
                        dataMap.getString("repositoryURI"))
                .usingJobData("breadcrumbProject",
                        dataMap.getString("breadcrumbProject"))
                .usingJobData("clusterName", dataMap.getString("clusterName"))
                .build();

        return retryTrigger;
    }
public boolean checkRetryShelf(final String merchantId, final String itemCatId) {
    
    final StateTrackerDAO<ShelfRobotStateTracker> sDAO = new StateTrackerDAO<>(ShelfRobotStateTracker.class);
    try {
        final ShelfRobotStateTracker stateTracker = sDAO.getByID(ShelfRobotStateTracker.getTrackerKey(itemCatId, merchantId));
        final Date rundate = new Date();
        return (stateTracker.retrieveExecutionInfo(new TSTimeStamp(rundate), false).isRetry());
    } catch (final PersistenceException e1) {
        // TODO Auto-generated catch block
        System.out.println(e1.getStackTrace());
    }
    return false;
}
}


