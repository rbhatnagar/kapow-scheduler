/**
 * 
 */
package com.quadanalytix.scheduler.main;

import com.kapowtech.robosuite.api.java.rql.ClusterAlreadyDefinedException;

import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quadanalytix.scheduler.robot.job.RoboServerException;
import com.quadanalytix.scheduler.robot.job.RobotJob;

/**
 * @author Faij
 * 
 */
public final class ServiceStart {

    /**
     * 
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceStart.class);

    private ServiceStart() {
    }

    public static void main(final String[] args) throws SchedulerException {

        LOGGER.info("Registering Cluster");
        try {
            RobotJob.registerCluster();
            LOGGER.info("Starting Kapow Scheduler");
           //Addition of Code to Start Scheduler Application
            
            
        } catch (final ClusterAlreadyDefinedException e) {
        
            LOGGER.error(e.getMessage());
        } catch (final RoboServerException e) {
        
            LOGGER.error(e.getMessage());
        }

        
    }
}
