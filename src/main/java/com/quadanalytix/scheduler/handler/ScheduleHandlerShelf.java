package com.quadanalytix.scheduler.handler;

import org.quartz.Job;

import com.quadanalytix.scheduler.robot.job.ShelfRobotJob;

public class ScheduleHandlerShelf extends AbstractScheduleHandler {
    
    ScheduleHandlerShelf() {
        super(RobotType.SHELF);
       
    }

    @Override
    protected Class<? extends Job> getJobClass() {
        return ShelfRobotJob.class;
     }
    

}
