package com.quadanalytix.scheduler.handler;

import org.quartz.Job;

import com.quadanalytix.scheduler.robot.job.ProductRobotRun;

public class ScheduleHandlerProduct extends AbstractScheduleHandler {

    ScheduleHandlerProduct() {
        super(RobotType.PRODUCT);
       
    }
    @Override
    protected Class<? extends Job> getJobClass() {
        // TODO Auto-generated method stub
        return ProductRobotRun.class;
    }

}
