package com.quadanalytix.scheduler.handler;

import org.quartz.Job;
import com.quadanalytix.scheduler.robot.job.BreadcrumbRobotRun;

public class ScheduleHandlerBreadCrumb extends AbstractScheduleHandler {

    
    ScheduleHandlerBreadCrumb() {
        super(RobotType.BREADCRUMB);
       
    }
    @Override
    protected Class<? extends Job> getJobClass() {
        // TODO Auto-generated method stub
        return BreadcrumbRobotRun.class;
    }

}
