package com.quadanalytix.scheduler.handler;

import static org.quartz.JobBuilder.newJob;

//import org.apache.commons.configuration.Configuration;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.CronScheduleBuilder.*;


import com.quadanalytix.scheduler.config.QuadRoboConfig;
import static com.quadanalytix.scheduler.config.QuadRoboConstants.*;

public abstract class AbstractScheduleHandler {
    
    private static final QuadRoboConfig ROBOCONFIG = QuadRoboConfig.getInstance(); 
    private String mRobotType;

     public enum RobotType {
       BREADCRUMB("bc"), PRODUCT("pd"), SHELF("sh");
       private final String mcode;
       
       RobotType(final String code) {
           mcode = code;
       }
       
       public String getCode() {
           return mcode;
       }
   };
    
    
    AbstractScheduleHandler(final RobotType robotypenum) {
    this.mRobotType = robotypenum.getCode();
    }
    
    
    public String getRoboType() {
        return (mRobotType);
    
    }
    
    public JobDetail createJobDetail(final String qrtzJobName) throws SchedulerException {
        
        final JobKey jobkey = new JobKey(qrtzJobName, mRobotType);
        JobDetail jobdetail = null;

        
            final Scheduler localscheduler = StdSchedulerFactory.getDefaultScheduler();
            if (!localscheduler.checkExists(jobkey)) {
               jobdetail = newJob(getJobClass())
                        .withIdentity(jobkey)
                        .usingJobData(TEMPDIR.toString(), ROBOCONFIG.getProperty(TEMPDIR.toString()))
                        .usingJobData(GETURI.toString(), ROBOCONFIG.getProperty(GETURI.toString()) + mRobotType)
                        .usingJobData(STOREURI.toString(), ROBOCONFIG.getProperty(STOREURI.toString()) + mRobotType)
                        .usingJobData(PROJECT.toString(), ROBOCONFIG.getProperty(PROJECT.toString() + mRobotType))
                        .usingJobData(ROBOTCOMPLETEDURL.toString(), ROBOCONFIG.getProperty(ROBOTCOMPLETEDURL.toString() + mRobotType))
                        .storeDurably().build();
                localscheduler.addJob(jobdetail, false);
                return (jobdetail);
            }
            return (localscheduler.getJobDetail(jobkey));
 }
 
   // Trigger Name T_MerchantID_ItemCatID, Group robot Type
   public Trigger createTrigger(final String qrtzTriggerName , final JobDetail jobdetail) throws SchedulerException {
       
       final Scheduler localscheduler = StdSchedulerFactory.getDefaultScheduler();
       
       final TriggerKey trigkey = new TriggerKey(qrtzTriggerName, mRobotType);
       if (!localscheduler.checkExists(trigkey)) {
          

           Trigger trigger = newTrigger().withIdentity(trigkey).withSchedule(dailyAtHourAndMinute(5, 2)).build();

           // Weekly Schedule
           trigger = newTrigger().withIdentity(trigkey).withSchedule(weeklyOnDayAndHourAndMinute(1, 5, 22)).build();
           // Daily Schedule
           trigger = newTrigger().withIdentity(trigkey).withSchedule(dailyAtHourAndMinute(5, 22)).build();
            
       }

  
return null;       
   
   }
   
 public String cronbuilder() {
     
     
    return null;
       
   }
    protected abstract Class<? extends Job> getJobClass();
}
