package com.quadanalytix.scheduler.handler;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;


public class JobDetailPersistance {
@Test
  public void breadcrumbcheck() {
      
      final ScheduleHandlerBreadCrumb schedulehandler = new ScheduleHandlerBreadCrumb();
      try {
        final JobDetail jobdetail = schedulehandler.createJobDetail("BreadCrumb_Test_Case_Check");
        final Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        
        final JobDetail jd2 = scheduler.getJobDetail(new JobKey("BreadCrumb_Test_Case_Check", schedulehandler.getRoboType()));
        
        assertTrue(jobdetail.equals(jd2));
        assertTrue(jobdetail.getKey().getName().equals("BreadCrumb_Test_Case_Check"));
        assertEquals(jobdetail.getJobDataMap().get("getURI"), jd2.getJobDataMap().get("getURI"));
        scheduler.deleteJob(new JobKey("BreadCrumb_Test_Case_Check", schedulehandler.getRoboType()));
        scheduler.shutdown();
      } catch (final SchedulerException e) {
        // TODO Auto-generated catch block
       
       
        }
      
      
  }
  
    
    
}
